/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment, Component} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    TextInput,
    Picker,
    Switch,
    Button,
} from 'react-native';

class App extends Component {
    state = {
        inputValue: '',
        todoList: [],
        switchValue: false
    };
    addToDo = () => {
        let todoList = this.state.todoList,
            {inputValue, switchValue} = this.state;
        if (inputValue.length === 0) {
            alert('Lütfen yapılacak gir');
        } else {
            todoList.push({inputValue: inputValue, isOpen: switchValue});
            this.setState({todoList});
        }
    };

    onChangeSwitch = () => {
        const {switchValue} = this.state;
        if (switchValue) {
            this.setState({switchValue:false})
        } else {
            this.setState({switchValue:true})
        }
    }

    render() {
        return (
            <Fragment>
                <SafeAreaView>
                    <View>
                        <TextInput placeholder={'Add to do'}
                                   style={{height: 50, width: 100, fontSize: 18}}
                                   onChangeText={(inputValue) => this.setState({inputValue})}
                                   id={'input'}/>
                        {/*<Picker*/}
                            {/*selectedValue={this.state.language}*/}
                            {/*style={{height: 50, width: 100}}*/}
                            {/*onValueChange={(itemValue, itemIndex) => */}
                                {/*this.setState({language: itemValue})*/}
                            {/*}>*/}
                            {/*<Picker.Item label="Java" value="java" />*/}
                            {/*<Picker.Item label="JavaScript" value="js" />*/}
                        {/*</Picker>*/}
                        <Switch onChange={() => this.onChangeSwitch()}
                                value={this.state.switchValue}/>
                        <Button style={{height: 50, width: 150, fontSize: 18, backgroundColor: 'red', color: 'white'}}
                                title={'EKLE'}
                                onPress={this.addToDo}>EKLE</Button>
                    </View>
                    <ScrollView
                        contentInsetAdjustmentBehavior="automatic"
                        style={styles.scrollView}>
                        {this.state.todoList.map(item => <Text style={styles.listItem} key={item.inputValue}>{item.inputValue} - Durumu: <Text style={styles.status}>{item.isOpen ? 'Açık' : 'Kapalı'}</Text></Text>)}

                    </ScrollView>
                </SafeAreaView>
            </Fragment>
        );
    }
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: '#FFF',
    },
    listItem: {
        fontSize: 24,
        fontWeight: "400"
    },
    status: {
        fontWeight: "700"
    }
});

export default App;
